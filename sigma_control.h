#include <Arduino.h>

class MyLibrary {
  private:
    byte* _template;
    int _templateSize;

  public:
    MyLibrary() {
      _template = nullptr;
      _templateSize = 0;
    }

    ~MyLibrary() {
      if (_template != nullptr) {
        delete[] _template;
      }
    }

    void clearedNewTemplate() {
      byte templateBytes[] = {0x7E, 0x31, 0x32, 0x38, 0x7E};
      int templateSize = sizeof(templateBytes) / sizeof(templateBytes[0]);

      // Звільнити попередній _template, якщо він існує
      if (_template != nullptr) {
        delete[] _template;
      }

      // Створити новий _template та скопіювати дані
      _templateSize = templateSize;
      _template = new byte[_templateSize];
      memcpy(_template, templateBytes, _templateSize);
    }
    
    void newTemplate() {
      byte templateBytes[] = {0x7E, 0x31, 0x32, 0x38, 0x7E, 0x66, 0x30, 0x31};
      int templateSize = sizeof(templateBytes) / sizeof(templateBytes[0]);

      // Звільнити попередній _template, якщо він існує
      if (_template != nullptr) {
        delete[] _template;
      }

      // Створити новий _template та скопіювати дані
      _templateSize = templateSize;
      _template = new byte[_templateSize];
      memcpy(_template, templateBytes, _templateSize);
    }

    void addText(String text, String color, String animation, String font) {
      const char fonts[] = {'r', 's', 't', 'u'};

      for (int i = 0; i < sizeof(fonts); i++) {
        if (font == String(fonts[i])) {
          font = "\\" + font;
          break;
        }
      }

      color = "\\" + color;
      String mergedText = animation + color + font + text;
      int mergedSize = mergedText.length();
      byte* mergedBytes = new byte[mergedSize];
      for (int i = 0; i < mergedSize; i++) {
        mergedBytes[i] = mergedText.charAt(i);
      }


      byte* newTemplate = mergeArrays(_template, _templateSize, mergedBytes, mergedSize);

      // Звільнити пам'ять попереднього _template
      delete[] _template;

      // Оновити _template та його розмір
      _template = newTemplate;
      _templateSize += mergedSize;

      delete[] mergedBytes;
    }

    void addBytes(byte* array1, int length) {
      byte* newTemplate = mergeArrays(_template, _templateSize, array1, length);
      delete[] _template;

      _template = newTemplate;
      _templateSize += length;
    }

    void newLine() {
      byte endMarker[] = {0x0D};
      byte* newTemplate = mergeArrays(_template, _templateSize, endMarker, 1);
      delete[] _template;

      // Оновити _template та його розмір
      _template = newTemplate;
      _templateSize += 1;
    }

    void endEdit() {
      byte endMarker[] = {0x0D, 0x0D, 0x0D};
      int endMarkerSize = sizeof(endMarker) / sizeof(endMarker[0]);

      // Об'єднати _template та endMarker
      byte* newTemplate = mergeArrays(_template, _templateSize, endMarker, endMarkerSize);

      // Звільнити пам'ять попереднього _template
      delete[] _template;

      // Оновити _template та його розмір
      _template = newTemplate;
      _templateSize += endMarkerSize;
    }

    void send() {
      Serial.begin(9600);
      for (int i = 0; i < _templateSize; i++) {
        Serial.println(_template[i], HEX);
      }
      Serial.write(_template, _templateSize);
    }

  private:
    byte* mergeArrays(byte* array1, int size1, byte* array2, int size2) {
      int mergedSize = size1 + size2;
      byte* mergedArray = new byte[mergedSize];

      memcpy(mergedArray, array1, size1);
      memcpy(mergedArray + size1, array2, size2);

      return mergedArray;
    }
};