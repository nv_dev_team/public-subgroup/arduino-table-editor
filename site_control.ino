#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <FS.h>
#include <sigma_control.h>
#include <ArduinoJson.h>

// Вказати дані вашої мережі WiFi
const char* ssid = "SigmaAS227 Controller";
const char* password = "47044319";
IPAddress local_IP(192,168,1,1);
IPAddress gateway(192,168,1,2);
IPAddress subnet(255,255,255,0);

MyLibrary editor;
ESP8266WebServer server(80);

byte* hexStringToByteArray(const String& input, size_t& outputSize) {
  size_t len = input.length();
  size_t byteCount = 0;
  byte* output = new byte[len / 3];

  for (size_t i = 0; i < len; i += 3) {
    String hexByte = input.substring(i, i + 2);
    output[byteCount] = strtol(hexByte.c_str(), NULL, 16);
    byteCount++;
  }

  outputSize = byteCount;
  return output;
}

void handleRoot() {
  File file = SPIFFS.open("/index.html", "r");
  if (!file) {
    server.send(404, "text/plain", "Файл не знайдено!");
    return;
  }

  String content = file.readString();
  file.close();
  server.send(200, "text/html", content);
}

void handleCSS() {
  File file = SPIFFS.open("/style.css", "r");
  if (!file) {
    server.send(404, "text/plain", "Файл не знайдено!");
    return;
  }

  String content = file.readString();
  file.close();
  server.send(200, "text/css", content);
}

void handleFont() {
  File file = SPIFFS.open("/jd_lcd_rounded.ttf", "r");
  if (!file) {
    server.send(404, "text/plain", "Файл не знайдено!");
    return;
  }

  String content = file.readString();
  file.close();
  server.send(200, "font/ttf", content);
}

void handleSend() {
  if (server.method() == HTTP_POST) {
    String dataArg = server.arg("data");
  
    size_t arraySize;
    byte* bytesOutput = hexStringToByteArray(dataArg, arraySize);
    editor.newTemplate();
    editor.addBytes(bytesOutput, arraySize);
    editor.endEdit();
    editor.send();
    
    server.send(200, "text/plain", "Sended");
  }
}

void handleBytes() {
  if (server.method() == HTTP_POST) {
    String dataArg = server.arg("data");
    
    size_t arraySize;
    byte* bytesOutput = hexStringToByteArray(dataArg, arraySize);
    editor.clearedNewTemplate();
    editor.addBytes(bytesOutput, arraySize);
    editor.endEdit();
    editor.send();
  }
}

void setup() {
  Serial.begin(9600);
  // Підключення до WiFi мережі
  WiFi.softAPConfig(local_IP, gateway, subnet);
  WiFi.softAP(ssid, password);

  Serial.println(WiFi.softAPIP());
  
  if (!SPIFFS.begin()) {
    return;
  }

  server.on("/", handleRoot);
  server.on("/style.css", handleCSS);
  server.on("/send", handleSend);
  server.on("/bytes", handleBytes);
  server.serveStatic("/jd_lcd_rounded.ttf", SPIFFS, "/jd_lcd_rounded.ttf");

  // Запуск веб-сервера
  server.begin();
}

void loop() {
  server.handleClient();
}
